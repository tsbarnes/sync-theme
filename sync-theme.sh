#!/bin/sh

## Variables
THEME="$1"
DRYRUN=""

## Functions
function init() {
    if [ -z "$THEME" ]; then
        THEME=$(gsettings get org.gnome.desktop.interface gtk-theme | sed s/\'//g)
    fi

    cd "$HOME/.config/gtk-4.0"

    if [ -d "$HOME/.themes/$THEME" ]; then
        THEME_DIR="$HOME/.themes/$THEME/gtk-4.0"
    elif [ -d "$HOME/.local/share/themes/$THEME" ]; then
        THEME_DIR="$HOME/.local/share/themes/$THEME/gtk-4.0"
    elif [ -d "/usr/share/themes/$THEME" ]; then
        THEME_DIR="/usr/share/themes/$THEME/gtk-4.0"
    else
        echo "Theme not found!"
        exit 1
    fi
}

function flatpak_override() {
    echo "Setting Flatpak GTK theme to '$THEME'..."
    sudo flatpak override --env="GTK_THEME=$THEME"
}

function remove_backups() {
    if [ -e "gtk.css~" ]; then
        echo "-- Removing old backups --"

        $DRYRUN rm -rf gtk.css~ gtk-dark.css~ assets~ apps~ widgets~ window-assets~
    fi
}

function move_existing() {
    echo "-- Moving existing links, if any --"

    if [ -e gtk.css ]; then
        echo "  gtk.css -> gtk.css~"
        $DRYRUN mv gtk.css gtk.css~
    fi
    if [ -e gtk-dark.css ]; then
        echo "  gtk-dark.css -> gtk-dark.css~"
        $DRYRUN mv gtk-dark.css gtk-dark.css~
    fi
    if [ -e assets ]; then
        echo "  assets -> assets~"
        $DRYRUN mv assets assets~
    fi
    if [ -e apps ]; then
        echo "  apps -> apps~"
        $DRYRUN mv apps apps~
    fi
    if [ -e widgets ]; then
        echo "  widgets -> widgets~"
        $DRYRUN mv widgets widgets~
    fi
    if [ -e window-assets ]; then
        echo "  window-assets -> window-assets~"
        $DRYRUN mv window-assets window-assets~
    fi
}

function link_theme() {
    echo "-- Linking new theme --"
    
    if [ ! -f "$THEME_DIR/gtk.css" ]; then
        echo "'gtk.css' not found!"
        exit 1
    fi
    if [ ! -f "$THEME_DIR/gtk-dark.css" ]; then
        echo "'gtk-dark.css not found!"
    fi
    
    echo "  $THEME_DIR/gtk.css -> ~/.config/gtk-4.0/gtk.css"
    $DRYRUN ln -s "$THEME_DIR/gtk.css" ./
    if [ -f "$THEME_DIR/gtk-dark.css" ]; then
        echo "  $THEME_DIR/gtk-dark.css -> ~/.config/gtk-4.0/gtk-dark.css"
        $DRYRUN ln -s "$THEME_DIR/gtk-dark.css" ./
    fi
    if [ -d "$THEME_DIR/assets" ]; then
        echo "  $THEME_DIR/assets -> ~/.config/gtk-4.0/assets"
        $DRYRUN ln -s "$THEME_DIR/assets" ./
    fi
    if [ -d "$THEME_DIR/apps" ]; then
        echo "  $THEME_DIR/apps -> ~/.config/gtk-4.0/apps"
        $DRYRUN ln -s "$THEME_DIR/apps" ./
    fi
    if [ -d "$THEME_DIR/widgets" ]; then
        echo "  $THEME_DIR/widgets -> ~/.config/gtk-4.0/widgets"
        $DRYRUN ln -s "$THEME_DIR/widgets" ./
    fi
    if [ -d "$THEME_DIR/window-assets" ]; then
        echo "  $THEME_DIR/window-assets -> ~/.config/gtk-4.0/window-assets"
        $DRYRUN ln -s "$THEME_DIR/window-assets" ./
    fi
}

## Main
init
remove_backups
move_existing
link_theme
flatpak_override

