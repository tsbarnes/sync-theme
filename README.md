# Sync Theme

A simple shell script to keep libadwaita and flatpak using your
custom GTK theme.

## Usage

```shell
sync-theme.sh
```
to detect the theme using gsettings, or:
```shell
sync-theme.sh <theme name>
```
to set it manually.